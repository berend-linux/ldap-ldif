## LDAP
These files are for adding users and groups to our LDAP server on the FDS01. These are template files and names need to be changed!
Soon a tool will do this automatically, but for now I add them to git in case I lose them somehow (took some time to figure out).

## UID
When adding users and groups it is important that each users gets his own group, for example for NVST I added group NVST with id 8001. When I add the user with id 8000 I add it to group 8001. Adding users, adding groups and adding users to groups are all seperate commands!

## More info
See our Wiki entry on LDAP for more configuration information + usefull commands -> http://wiki.nugtr.nl/index.php?title=LDAP. Or check the documentation for our entire server network for even more in depth information.